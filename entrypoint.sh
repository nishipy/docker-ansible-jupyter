#!/bin/sh
echo "Serving JupyterLab ..."
cd /home/${JP_USER}
jupyter notebook --NotebookApp.password="" --NotebookApp.token="" --no-browser --port=${JP_PORT} --ip=0.0.0.0