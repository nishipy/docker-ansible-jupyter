FROM alpine:3.11

LABEL maintainer='nishipy.com'

ENV LANG=ja_JP.UTF-8 JP_PORT=8888 JP_USER=jupyter

EXPOSE ${JP_PORT}

RUN apk update && apk upgrade && apk add \
        curl \
        python3 \
        python3-dev \
        git \
        wget \
        musl \
        linux-headers \
        gcc \
        g++ \
        make \
        build-base \
        libffi-dev \ 
        libressl-dev && \
    pip3 install --no-cache-dir --upgrade pip && \
    pip3 install  --no-cache-dir jupyter jupyterlab ansible && \
    adduser ${JP_USER} --home /home/${JP_USER} --shell /bin/bash -D

ADD entrypoint.sh /home/${JP_USER}
RUN chown ${JP_USER} /home/${JP_USER}/entrypoint.sh && \
    chmod a+x /home/${JP_USER}/entrypoint.sh

USER ${JP_USER}
WORKDIR /home/${JP_USER}
ENTRYPOINT [ "./entrypoint.sh" ]