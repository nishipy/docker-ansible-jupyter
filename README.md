![](https://gitlab.com/nishipy/docker-ansible-jupyter/badges/master/pipeline.svg)
# docker-ansible-jupyter
Here is my docker image to run ansible and jupyter internally.
For more detail, see [this post](https://nishipy.com/archives/1409).

## How to build
You can build the docker image simply via [Dockerfile](./Dockerfile).
```shell
cd docker-jupyter-ansible
docker build -t ansible-jupyter .
```

## How to use
Please run `docker run` command and you will see output from [entrypoint.sh](./entrypoint.sh).
```shell
$ docker run --name test -p 8889:8888 ansible-jupyter
```

After that, you can access `localhost:8889` via browser ans use jupyter notebook.

![jupyter](https://nishipy.com/wp-content/uploads/2020/04/%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%BC%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%83%E3%83%88-2020-04-25-22.36.25-1024x274.jpg)